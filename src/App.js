import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { initApp } from './actions/init';

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ initApp }, dispatch)
  };
};
class App extends React.Component {
  static propTypes = {
    actions: PropTypes.shape({
      initApp: PropTypes.func
    }).isRequired
  }

  constructor(props) {
    super(props);
    props.actions.initApp();
  }

  render() {
    return (
      <div />
    );
  }
}

export default connect(() => ({}), mapDispatchToProps)(App);
