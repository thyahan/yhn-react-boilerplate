const DEVELOP_BASE_URL = 'localhost:8080';
const base = window.config.BASE_URL || DEVELOP_BASE_URL;

export const INIT = `${base}/init`;
