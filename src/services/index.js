// import { GET } from './api';
// import { INIT } from './apiList';

// const getInitApp = () => GET(INIT);
const getInitApp = () => {
  return {
    responseData: [],
    status: 'SUCCESS'
  };
};

export {
  getInitApp
};
