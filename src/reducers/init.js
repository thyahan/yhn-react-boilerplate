import _ from 'lodash';
import { INIT_APP } from '../actions/type';

const initialState = {
  username: '',
  token: ''
};

export default (state = initialState, action) => {
  switch (action.type) {
    case INIT_APP:
      return _.assign({}, state, {
        username: _.get(action, 'username', 'simple_user'),
        token: _.get(action, 'token', 'god_is_token')
      });
    default:
      return state;
  }
};
