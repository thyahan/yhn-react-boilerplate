import { INIT_APP, ERROR } from './type';
import * as Services from '../services';

export const initApp = () => {
  return async (dispatch) => {
    try {
      const { responseData, status } = await Services.getInitApp();
      if (status === 'SUCCESS') {
        return dispatch({
          type: INIT_APP,
          data: responseData
        });
      }
      return dispatch({
        type: ERROR,
        data: 'Cannot connect to server',
        errorMessage: ''
      });
    } catch (e) {
      return dispatch({
        type: ERROR,
        data: 'Cannot connect to server',
        errorMessage: e.message
      });
    }
  };
};
